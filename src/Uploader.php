<?php

namespace Hermes\Uploader;

use Storage;
use Illuminate\Http\UploadedFile;

class Uploader
{
    /**
     * Upload
     *
     * @param       UploadedFile    The file we're uploading
     * @param       String          Path to upload the file to relative to the public folder        example: "images/awesomesauce" so no "/" suffix
     * @param       String          The desired name of the file (without extension)
     * @param       String          The storage filesystem to upload the file to
     * @return      String          Relative path to the uploaded file
     */
    public function upload(UploadedFile $file, $upload_path = null, $file_name = null, $filesystem = "public")
    {
        # Make sure we have a path to upload the file to
        if (is_null($upload_path)) $upload_path = "uploads";

        # Convert the relative path to an absolute path
        // $absolute_file_path = public_path() . "/" . $upload_path;
        
        # Make sure the directory the absolute file path is pointing towards exists
        if (!Storage::exists($upload_path)) Storage::makeDirectory($upload_path);

        # If we didn't receive a specific file name
        if (is_null($file_name))
        {
            # Move the file and let laravel generate a random name
            $path = $file->store($upload_path, $filesystem);
        }
        # If we received a specific file name
        else
        {
            # Move the file using the received name
            $path = $file->storeAs($upload_path."/".$file_name,  $filesystem);
        }

        # Return the uploaded file's final path (relative instead of absolute)
        return "storage/".$path;
    }

    /**
     * Generate file name
     * 
     * @param       String          The extension the generated file name should have
     * @param       String          The file name without the extension (in case we just want append ext)
     * @return      String
     */
    private function generateFileName($extension, $file_name = null)
    {
        // If no file name was specified; generate one
        if (is_null($file_name))
        {
            $file_name = substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", mt_rand(0, 50), 1).substr(md5(time()), 1);
        }
        
        // Return the file name incl. it's extension
        return $file_name . "." . $extension;
    }
}