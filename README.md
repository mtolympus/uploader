# Laravel Uploader

A simple service which helps with processing image uploads in forms.

# Installation

Require the package in your application using the following command:

```
composer require hermes/uploader
```

You're done!

# Usage

You can use the Uploader facade in your controllers like this:

```
<?php

namespace App\Http\Controllers;

use Uploader;

class UploadTestController extends Controller
{
    public function postTestMethod(TestRequest $request)
    {
        if ($request->hasFile('image'))
        {
            Uploader::upload($request->file('image'), 'storage/images');
        }

        // .. do other stuff
    }
}
```

The upload methods accepts the following parameters:

... todo ..